//console.log("Hello, 204!");

//Functions
	/*
		Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoked.

		Functions are mostly created to create complicated tasks to run several lines of codes.

	*/

//Function Declarations
	//(function statement) defines a function with a specified parameters

	/*
		Syntax:
			function functionName() {
					code block(statement);
			}

		function keyword - used ti defined a javascript functions
		functionName - the function name. Functions are named to be able to use later in the code.
		function block ({}) - the statement which comprise the body of the function. This where the code to be executed.
	*/


	function printName() {
		console.log("My name is John!");
	};

//Function Invocation
	/*
	
	The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.

	*/

	//Let's invoke/call the functions that we declared.
	printName();

	//declaredFunction(); // error - we cannot invoke a function we have yet to define


//Function Declarations vs Function Expression
	//Function Declarations

	//A function can be created through function declaration by using the function keyword and adding a function name.

	//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
			
	declaredFunction(); //declared functions can be hoisted. As long as the function has been declared

	//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.


	function declaredFunction() {
		console.log("Hello, from the declaredFunction()");
	};

	declaredFunction();



	//Function Expression
		//A function can be also stored in a variable. This is called a function expression.

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name.

		//variableFunction(); Cannot access 'variableFunction' before initialization
		//error - function expressions, being stored in a let or const variable, cannot be hoisted.


		let variableFunction = function() {

			console.log("Hello Again!");
		}

		variableFunction();

		// We can also create a function expression of a named function.
		// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
		// Function Expressions are always invoked (called) using the variable name.


		let funcExpression = function funcName() {
			console.log("Hello from the other side!");
		}

		funcExpression();
		//funcName(); //error


	//You can reassign declared function and function expression to new anonymous function
		declaredFunction = function() {
			console.log("Updated declaredFunction");

		}

		declaredFunction();


		funcExpression = function() {
			console.log("Updated funcExpression");
		}

		funcExpression();

	//However, we cannot re-assign a function expression initialized with const.

	const constantFunc = function() {
		console.log("Initialized with const");
	}

	constantFunc();

	// constantFunc = function() {
	// 	console.log("Can we reassigned?");
	// }

	// constantFunc();


//Function Scoping
/*
	Scope is the accessibility (visibility) of variables within our program.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope

*/

	{
		let localVar = "Juan Dela Cruz";
	}

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);
	//console.log(localVar);

//Function Scope
/*		
	JavaScript has function scope: Each function creates a new scope.
	Variables defined inside a function are not accessible (visible) from outside the function.
	Variables declared with var, let and const are quite similar when declared inside a function.
*/


	function showNames() {

		var functionVar = "Mark";
		const functionConst = "Jayson";
		let functionLet = "Arnel";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);


// Nested Functions
	//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

	function myNewFunction() {
		let name = "Gabriella";

		function nestedFunction() {
			let nestedName = "Maria";
			console.log(name);
		}

		nestedFunction();

		//console.log(nestedName); //results an error
		//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in/
	}

	myNewFunction();

	//nestedFunction(); //results an error
	// Moreover, since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in.



//Function and Global Scoped Variables

	//Global Scoped Variable
	let globalName = "Alexandro";

	function myNewFunction2() {
		let nameInside = "Khabib";

		//Variables declared Globally (outside any function) have Global scope.
		//Global variables can be accessed from anywhere in a Javascript 
		console.log(globalName);

	}

	myNewFunction2();

	console.log(globalName);
	//console.log(nameInside); //results an error
	//nameInside is function scoped. It cannot be accessed globally.


//Using Alert()

	//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.


	//alert() syntax:
	//alert("<messageInString>");


	alert("Hello, JavaScript!");

	//You can use an alert() to show a message to the user from a later function invocation.
	function showSampleAlert() {
		alert("Hello, User!");
	}

	showSampleAlert();


	//You will find that the page waits for the user to dismiss the dialog before proceeding. You can witness this by reloading the page while the console is open.
	console.log("I will only log in the console when the alert is dismissed");

	//Notes on the use of alert():
		//Show only an alert() for short dialogs/messages to the user. 
		//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.



//Using Prompt
	// prompt() allows us to show a small window at the top of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from prompt() will be returned as a String once the user dismisses the window.


/*
	Syntax:
		prompt("<dialogInString>");

*/

let samplePrompt = prompt("Enter your name.");

console.log("Hello, "+ samplePrompt); //The value of the user input from a prompt is returned as a string.

//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.



//Let's create function scoped variables to store the returned data from our prompt(). This way, we can dictate when to use a prompt() window or have a reusable function to use our prompts.

function printWelcomeMessage() {

	let firstName = prompt("Enter Your First Name.");
	let lastName = prompt("Enter Your Last Name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	alert("Welcome to my page!");

}

printWelcomeMessage();


//Function Naming Conventions
	//Function names should be definitive of the task it will perform. It usually contains a verb.

	function getCourses() {

		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();



	//Avoid generic names to avoid confusion within your code
	function get() {

		let name = "Jamie";
		console.log(name);

	}

	get();


	//Avoid pointless and inappropriate function names

	function foo () {
		console.log(25%5);
	}

	foo();


	//Name your functions in small caps. Follow camelCase when naming variables and functions

	function displayCarInfo() {

		console.log("Brand: Toyata");
		console.log("Type: Sedan");
		console.log("Price: 1,500,00");

	}


	displayCarInfo();


	function display_info() {

		let first_name = "Jack Mendoza"
		
		console.log(first_name);
	}


	display_info();